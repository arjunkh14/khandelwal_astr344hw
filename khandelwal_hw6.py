import numpy as np
import time
from khandelwal_hw5 import roots #Importing the bisection function from my previous homework
from math import exp

def derivative(f, x):
    h = 0.000001
    df = (f(x + h)-f(x))/h
    return df

def nr(f, x):
    return x - (f(x)/derivative(f, x))

def f(x):
    return np.sin(x)
def g(x):
    return (x**3) - x - 2
def y(x):
    return -6 + x + (x**2)

tol = 10**(-3)

#Running NR and bisection for f(x), g(x), and y(x), and comparing times
startgnr = time.time()
xi = 3
while g(xi) > tol:
    xi = nr(g, xi)
endgnr = time.time()
timegnr = endgnr - startgnr
print "Time to get root " + str(xi) + " for g(x) using NR is: ", timegnr

startgb = time.time()
roots(g, 1, 2)
endgb = time.time()
timegb = endgb - startgb
print "Time to get root " + str(xi) + " for g(x) using bisection is: ", timegb

if timegnr < timegb:
    print "NR took less time than B for g(x)"
else: 
    print "B took less time than NR for g(x)"

startfnr = time.time()
xi = 3
while f(xi) > tol:
    xi = nr(f, xi)
endfnr = time.time()
timefnr = endfnr - startfnr
print "Time to get root " + str(xi) + " for f(x) using NR is: ", timefnr

startfb = time.time()
roots(f, 2, 4)
endfb = time.time()
timefb = endfb - startfb
print "Time to get root " + str(xi) + " for f(x) using bisection is: ", timefb

if timefnr < timefb:
    print "NR took less time than B for f(x)"
else: 
    print "B took less time than NR for f(x)"

startynr = time.time()
xi = 3
while y(xi) > tol:
    xi = nr(y, xi)
endynr = time.time()
timeynr = endynr - startynr
print "Time to get root " + str(xi) + " for y(x) using NR is: ", timeynr

startyb = time.time()
roots(y, 0, 5)
endyb = time.time()
timeyb = endyb - startyb
print "Time to get root " + str(xi) + " for y(x) using bisection is: ", timeyb

if timeynr < timeyb:
    print "NR took less time than B for y(x)"
else:
    print "B took less time than NR for y(x)"

print "When I did this a few times, NR was faster for all three functions with initial guess = 3 for all"

#Blackbody Question

#Defining constants
h = 6.6261e-27
c = 2.9979e10
k = 1.3807e-16
l = .0870
v = 3.4459e10
v = c/l

#Defining the funciton which we want the root of
def b(t):
    return -(1.25e-12) + (2.0 * h *( (v**3)/ (c**2) ))/(exp( (h * v)/(k * t))-1.0)

tol = 10e-25
xi = 30
start = time.time()
while abs(b(xi)) > tol:
    xi = nr(b, xi)
end = time.time()

print "The temp of the dust is " + str(xi), " time = ", str(end - start)


