import numpy as np

a = np.float64(1.)
r = np.float64(1./3)
n = 1
sum64_5 = a
while n < 6:
    n += 1
    a = a * r
    sum64_5 = sum64_5 + a
print "The sum of %s iterations of float64 is %s." % (n-1, sum64_5)

a = np.float64(1.)
r = np.float64(1./3)
n = 1
sum64_20 = a
while n < 21:
    n += 1
    a = a * r
    sum64_20 += a
print "The sum of %s iterations of float64 is %s." % (n-1, sum64_20)

a = np.float32(1.)
r = np.float32(1./3)
n = 1
sum32_5 = a
while n < 6:
    n += 1
    a = a * r
    sum32_5 += a
print "The sum of %s iterations of float32 is %s." % (n-1, sum32_5)

a = np.float32(1.)
r = np.float32(1./3)
n = 1
sum32_20 = a
while n < 21:
    n += 1
    a = a * r
    sum32_20 += a
print "The sum of %s iterations of float32 is %s." % (n-1, sum32_20)

abser_5 = sum32_5 - sum64_5
abser_20 = sum32_20 - sum64_20
reler_5 = -(abser_5/sum64_5)
reler_20 = -(abser_20/sum64_20)

print "Abs error, 5 iterations = %s, Rel error, 5 iterations = %s, Abs error, 20 iterations = %s, Rel error, 20 iterations = %s." % (abser_5, reler_5, abser_20, reler_20)

#######

print "Now using x_n = 4^n"

a = np.float64(4.)
r = np.float64(4.)
n = 0
sum64_5_4 = 0
while n < 5:
    n += 1
    sum64_5_4 = sum64_5_4 + a
    a = a * r
#    print "n = %s, sum = %s, float 64" % (n, sum64_5_4)
print "The sum of %s iterations of float64 is %s." % (n, sum64_5_4)

a = np.float64(4.)
r = np.float64(4.)
n = 0
sum64_20_4 = 0
while n < 20:
    n += 1
    sum64_20_4 += a
    a = a * r
print "The sum of %s iterations of float64 is %s." % (n, sum64_20_4)

a = np.float32(4.)
r = np.float32(4.)
n = 0
sum32_5_4 = 0
while n < 5:
    n += 1
    sum32_5_4 += a
    a = a * r
#    print "n = %s, sum = %s, float 32" % (n, sum32_5_4)
print "The sum of %s iterations of float32 is %s." % (n, sum32_5_4)

a = np.float32(4.)
r = np.float32(4.)
n = 0
sum32_20_4 = 0
while n < 20:
    n += 1
    sum32_20_4 += a
    a = a * r
print "The sum of %s iterations of float32 is %s." % (n, sum32_20_4)

abser_5_4 = sum32_5_4 - sum64_5_4
abser_20_4 = sum32_20_4 - sum64_20_4
reler_5_4 = -(abser_5_4/sum64_5_4)
reler_20_4 = -(abser_20_4/sum64_20_4)

print "Abs error, 5 iterations = %s, Rel error, 5 iterations = %s, Abs error, 20 iterations = %s, Rel error, 20 iterations = %s." % (abser_5_4, reler_5_4, abser_20_4, reler_20_4)

print "We can see that this calculation is stable since it doesn't involve using so many decimal places, which means it doesn't run into issues with the difference in decimal places in float 32 and float 64."

print "In general, absolute and relative error are complementary tools to measure accuracy and stability. If I had to use only one, I would use relative error however, since it scales the error according to the final answer and lets you know how big your error is relative to your answer." 
