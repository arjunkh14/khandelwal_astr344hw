import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

#coordinates takes the list of people and returns the x and y values for plotting. It also creates a separate array for the cooordinates of infected people so I can overplot them in a different color.

def coordinates(array):
    xvalues, yvalues, xcolor, ycolor = [], [], [], []
    for i in range(len(array)):
        xvalues.append(array[i][0])
        yvalues.append(array[i][1])
        if infected[i] == 1:
            xcolor.append(array[i][0])
            ycolor.append(array[i][1])
    return xvalues, yvalues, xcolor, ycolor

#createpeople creates an array of N random people, infects inf of them, creates arrays to track time steps and total infected people, and returns these values

def createpeople(N, inf, vac):
    people = []
    for i in range(N):
        x = np.random.uniform(0, N)
        y = np.random.uniform(0, N)
        position = [x, y]
        people.append(position)
   #I create an array of 0's for all the people and decide with a for loop how many should start out infected (in which case the value of infected[i] is 1).
    infected = np.zeros(len(people))
    vaccinated = np.zeros(len(people))
    for i in range(inf):
        infected[i] = 1
    for j in range(inf, inf + vac):
        vaccinated[j] = 1
    return N, people, infected, vaccinated

def movepeople(N, people, infected, vaccinated):
    suminf = sum(infected)
    vac = sum(vaccinated)
    t = 0
    time = []
    while suminf < (N - vac):
        suminf = sum(infected)
        initpos = deepcopy(people)
        for i in range(len(people)):
            xvel = np.random.uniform(-velocity, velocity) #Random x velocity
            yvel = np.random.uniform(-velocity, velocity) #Random y velocity
            if (initpos[i][0] + xvel) > N: #Lines 48-55 are for bouncing off walls
                initpos[i][0] = 2*N - initpos[i][0] - xvel
            if (initpos[i][1] + yvel) > N: 
                initpos[i][1] = 2*N - initpos[i][1] - yvel
            if (initpos[i][0] + xvel) < 0: 
                initpos[i][0] = abs(xvel) - initpos[i][0]
            if (initpos[i][1] + yvel) < 0: 
                initpos[i][1] = abs(yvel) - initpos[i][1]
            if [initpos[i][0] + xvel, initpos[i][1] + yvel] in initpos:
                print "Collision!"
                xvel = np.random.uniform(-velocity, velocity)
                yvel = np.random.uniform(-velocity, velocity)
            else:
                initpos[i][0] += xvel #If they aren't too close to the walls
                initpos[i][1] += yvel
                initx, inity, initxc, inityc = coordinates(initpos)
                xpos = initx[i]*np.ones(len(initpos)) #xpos has everyone's x position                
                ypos = inity[i]*np.ones(len(people)) #ypos has everyone's y position
                dist = np.sqrt((xpos - initx)**2 + (ypos - inity)**2) #Calculate everyone's distance from person i
                tooclose = np.where(dist<1)[0]
#                print len(tooclose)
                while len(tooclose) > 1:
#                    print "TOO CLOSE AAAAHHHHHHHHHHHH GET OUT OF MY SPACE"
#                    print len(tooclose)
                    xvel = np.random.uniform(-velocity, velocity)
                    yvel = np.random.uniform(-velocity, velocity)
                    if (initpos[i][0] + xvel) > N: #The next several lines are again for bouncing off walls
                        initpos[i][0] = 2*N - initpos[i][0] - xvel
                    if (initpos[i][1] + yvel) > N: 
                        initpos[i][1] = 2*N - initpos[i][1] - yvel
                    if (initpos[i][0] + xvel) < 0: 
                        initpos[i][0] = abs(xvel) - initpos[i][0]
                    if (initpos[i][1] + yvel) < 0: 
                        initpos[i][1] = abs(yvel) - initpos[i][1]
                    if [initpos[i][0] + xvel, initpos[i][1] + yvel] in initpos:
                        print "Collision!"                        
                    else:
                        initpos[i][0] += xvel #If they aren't too close to the walls
                        initpos[i][1] += yvel
                    initx, inity, initxc, inityc = coordinates(initpos)
                    xpos = initx[i]*np.ones(len(initpos)) #xpos has everyone's x position      
                    ypos = inity[i]*np.ones(len(people)) #ypos has everyone's y position          
                    dist = np.sqrt((xpos - initx)**2 + (ypos - inity)**2) #Calculate everyone's distance from  person i
                    tooclose = np.where(dist<1)[0]        
        people = initpos
        xvalues, yvalues, xcolor, ycolor = coordinates(people)
        for i in range(len(people)):
            if infected[i] == 1:
                xpos = xvalues[i]*np.ones(len(people)) #xpos has everyone's x position
                ypos = yvalues[i]*np.ones(len(people)) #ypos has everyone's y position
                dist = np.sqrt((xpos - xvalues)**2 + (ypos - yvalues)**2) #Calculate everyone's distance from infected person i
                infect = np.where(dist<2)[0] #Get indices where distance is too close
                for j in infect: #Infect them with 75% probability
                    p = np.random.random()
                    if (p < 0.75) & (vaccinated[j] == 0):
                        infected[j] = 1
        time.append(t+1)
        t += 1
#        print t
    return t, suminf

'''
N, people, infected, vaccinated = createpeople(100, 1, 0)
velocity = (N/25.0)
t, suminf = movepeople(N, people, infected, vaccinated)
print t, suminf



'''
fmean, fstd, numbervac = [], [], []
for i in range(20):    
    print i
    finalval = []
    for j in range(10):
        N, people, infected, vaccinated = createpeople(100, 1, i*5)
        velocity = (N/25.0) #A person can cross the room in 25 steps
        t, suminf = movepeople(N, people, infected, vaccinated)
        finalval.append(t)
#        print j
    fmean.append(np.mean(finalval))
    fstd.append(np.std(finalval))
    numbervac.append(i*5)
#print finalval, np.mean(finalval), np.std(finalval)
print fmean, fstd, numbervac

plt.errorbar(numbervac, fmean, yerr = fstd, fmt = '--o', color = 'r', ecolor = 'black')
plt.title('Time to infect everyone vs number of people vaccinated with buffer')
plt.xlabel('Number of people vaccinated')
plt.ylabel('Time steps to infect everyone')
plt.savefig('infecttime-vs-vacno-100iterations-buffer.eps')
plt.show()

#print finalval


"""



#pdfinf is used to get the number of new infections at each timestep
pdfinf = []
for i in range(len(totalinf)-1):
    pdfinf.append(totalinf[i+1] - totalinf[i])

pdfinf.append(0)
pdfinf = np.array(pdfinf)
time = np.array(time)

#Plotting Total Number of People Infected vs Time
plt.plot([], [], linewidth = 0, label = "Total number of people = " + str(N))
plt.plot(time, totalinf, linewidth = 2, label = 'Number of People Infected', color = 'r')
plt.ylabel('Total Number of people infected')
plt.xlabel('Time steps')
plt.ylim(0, 100)
plt.title('Total Number of People Infected vs Time')
plt.legend(loc = 'lower right', prop={'size':10})
plt.show()

#Plotting the PDF of Infecting People
plt.bar(time, pdfinf, color = 'black')
plt.title("Number of New People Infected at Each Time Step")
plt.xlabel("Time Step")
plt.ylabel("New People Infected")
plt.show()
"""
