import numpy as np
import matplotlib.pyplot as plt

H2, SFR = np.loadtxt('ks_observables.dat', skiprows = 1, usecols = (0, 1), unpack = True)

logH2 = np.log10(H2)
logSFR = np.log10(SFR)

#print logH2[:5], logSFR[:5]

def lse(x, y):
    sumx = np.sum(x)
    sumy = np.sum(y)
    sumxy = np.sum(x*y)
    sumx2 = np.sum(x*x)
    a1 = ((sumy)*(sumx2) - (sumx)*(sumxy))/((sumx2) - ((sumx)**2))
#    a2 = (sumxy - ((sumy)*(sumx)))/(sumx2 - ((sumx)**2))
    a2 = np.mean(y) - (a1 * np.mean(x))
    return a1, a2

b1, b2 = lse(logH2, logSFR)
print "slope = ", str(b1), "intercept = ", str(b2)

xaxis = np.linspace(np.min(logH2), np.max(logH2), 10000)
fit = b1*xaxis + b2

plt.scatter(logH2, logSFR)
plt.title('Star Formation Rate vs CO Brightness')
plt.xlabel('log10(CO Brightness)')
plt.ylabel('log10(Star Formation Rate)')
plt.plot(xaxis, fit, c = 'red', label = 'Least Squares Regression')
#plt.legend(loc = 'upper left')
plt.legend()
plt.show()
