import numpy as np
from trapint import trapint
import matplotlib.pyplot as plt

def r(z): #Defining the r(z) function
    c = 3000.0
    Hz = ((0.3)*((1+z)**3) + 0.7)**0.5
    rz = c/Hz
    return rz

#Defining the array of z values to be used from z=0 and z=5
zrange = np.arange(0, 5.1, 0.1)

n = 100 #The number of trapezoids in each integral

#Now I define a lot of empty arrays to be used to construct the plot
fxi = []
xaxis = []
da_d = []
int = []
sum = []

#Looping over all z values
for i in zrange:
    xaxis.append(i)
    xi = np.linspace(0, i, n)
    for x in xi:
        fxi.append(r(x))
    sum = trapint(xi, fxi)
    da_d.append(1 + i)
    int.append(sum)
    fxi = []

da_d = np.array(da_d)
int = np.array(int)
da = int/da_d

plt.plot(xaxis, da, linewidth = 7, label = 'Homemade Trapezoidal Integrator', alpha = 0.7)
   
#Now we compare to the Numpy Trapz integrator by redefining the arrays to be empty, running the same loop except using the Numpy.trapz integrator instead of my crude homemade one, and plotting. 

fxi, int, sum, da_n, da_d = [], [], [], [], []

for i in zrange:
    xi = np.linspace(0, i, n)
    for x in xi:
        fxi.append(r(x))
    sum = np.trapz(fxi, x=xi)
    da_d.append(1 + i)
    int.append(sum)
    fxi = []

da_d = np.array(da_d)
int = np.array(int)
da_n = int/da_d

plt.plot(xaxis, da_n, linewidth=2, label = 'Professional Numpy Trapz Integrator')
plt.title("Solving the fundamental questions in astronomy one homework at a time")
plt.xlabel("z")
plt.ylabel("$D_{A}$")
plt.legend(prop={'size':10})
plt.show()
plt.savefig('khandelwal_hw4.jpg')
