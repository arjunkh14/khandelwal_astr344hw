import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math

#lum1 = np.genfromtxt('model_smg.dat', usecols = (0), dtype = None)
#num1 = np.genfromtxt('model_smg.dat', usecols = (1), dtype = None)
model_l = np.array([1.1, 2.3, 3.4, 4.6, 5.7, 6.9, 8.0, 9.1, 10.3, 11.5, 12.6, 13.8])
model_n = np.array([8038, 2676, 773, 354, 200, 110, 65, 41, 27, 13, 6, 2])

#Defining h1 and h2 for n and l and filling in the edge values
l_h1 = np.zeros([len(model_l)])
l_h1[0] = model_l[0]

l_h2 = np.zeros([len(model_l)])
l_h2[len(model_l)-1] = model_l[len(model_l)-1]-model_l[len(model_l)-2]

#Defining a1, a2, and a3 for n and l
l_a1 = np.zeros([len(model_n)])
l_a2 = np.zeros([len(model_n)])
l_a3 = np.zeros([len(model_n)])

#Defining dN/dL 
dn_dl = np.zeros([len(model_n)])

#Now filling in h1 and h2 for n and l
for i in np.arange(1,len(model_l)):
    l_h1[i] = model_l[i] - model_l[i-1]

for i in np.arange(0,len(model_l)-1):
    l_h2[i] = model_l[i+1]-model_l[i]

#Filling in a1, a2, and a3 for n and l
for i in np.arange(0, len(model_n)):
    l_a1[i] = (l_h1[i])/((l_h2[i])*(l_h1[i] + l_h2[i])) 

for i in np.arange(0, len(model_n)):
    l_a2[i] = (l_h1[i]-l_h2[i])/((l_h2[i])*(l_h1[i])) 

for i in np.arange(0, len(model_n)):
    l_a3[i] = (l_h2[i])/((l_h1[i])*(l_h1[i] + l_h2[i])) 

l = len(model_l) - 1

dn_dl[0] = (l_a1[0])*(model_n[1]) - ((l_a2[0])*(model_n[0])) - 0
dn_dl[l] =  -((l_a2[l])*(model_n[l])) - ((l_a3[l])*(model_n[l-1]))

for i in range(1, l):
    dn_dl[i] = (l_a1[i])*(model_n[i+1]) - ((l_a2[i])*(model_n[i])) - ((l_a3[i])*(model_n[i-1]))
                                    
abs_dndl = []
for x in dn_dl:
    abs_dndl.append(abs(x))

lum_dndl = np.log10(abs_dndl)

log_lum = []
for x in model_l:
    log_lum.append(np.log10(x))

data = np.loadtxt('ncounts_850.dat')
data_l = data[:, 0]
data_n = data[:, 1]

#import pdb; pdb.set_trace()

x = range(-1, 3)
y = range(-5, 7)
fig = plt.figure()
ax1 = fig.add_subplot(111)

ax1.scatter(log_lum, lum_dndl, s=75, marker='s', c = 'b', label = 'model')
ax1.scatter(data_l, data_n, s = 20, marker = 'o', c = 'r', label = 'observed')
plt.legend(loc = 'upper left');
#plt.show()

plt.savefig('Astro344HW3.jpg')

#import pdb; pdb.set_trace()

#print lum_dndl





