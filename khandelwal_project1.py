import numpy as np
import matplotlib.pyplot as plt


#coordinates takes the list of people and returns the x and y values for plotting. It also creates a separate array for the cooordinates of infected people so I can overplot them in a different color.

def coordinates(array):
    xvalues, yvalues, xcolor, ycolor = [], [], [], []
    for i in range(len(array)):
        xvalues.append(array[i][0])
        yvalues.append(array[i][1])
        if infected[i] == 1:
            xcolor.append(array[i][0])
            ycolor.append(array[i][1])
    return xvalues, yvalues, xcolor, ycolor

#createpeople creates an array of N random people, infects inf of them, creates arrays to track time steps and total infected people, and returns these values

def createpeople(N, inf):
    people = []
    for i in range(N):
        x = np.random.uniform(0, N)
        y = np.random.uniform(0, N)
        position = [x, y]
        people.append(position)
   #I create an array of 0's for all the people and decide with a for loop how many should start out infected (in which case the value of infected[i] is 1).
    infected = np.zeros(len(people))
    for i in range(inf):
        infected[i] = 1
    totalinf, time = [inf], [0] #Empty arrays in which I can track the number of people infected over time
    return N, people, infected, totalinf, time


N, people, infected, totalinf, time = createpeople(100, 1)

#Plotting the initial positions of everyone
xvalues, yvalues, xcolor, ycolor = coordinates(people)
fig = plt.figure()
ax1 = fig.add_subplot(111)
y1 = ax1.scatter(xvalues, yvalues, c = 'green')
y2 = ax1.scatter(xcolor, ycolor, c = 'red') #I overplot the infected people with red
plt.legend((y1, y2), ('Healthy', 'Infected'), scatterpoints = 1)
ax1.set_title("Initial Positions without buffer")
ax1.set_xlabel('X dimension')
ax1.set_ylabel('Y dimension')
plt.xlim(0, N)
plt.ylim(0, N)
#plt.show()


velocity = (N/25.0) #A person can cross the room in 25 steps
#timesteps = 250
suminf = sum(infected)
t = 0
while suminf < 100:
#for t in range(timesteps):
    suminf = sum(infected)
    for i in range(len(people)):
        xvel = np.random.uniform(-velocity, velocity) #Random x velocity
        yvel = np.random.uniform(-velocity, velocity) #Random y velocity
        if (people[i][0] + xvel) > N: #Lines 48-55 are for bouncing off walls
            people[i][0] = 2*N - people[i][0] - xvel
        if (people[i][1] + yvel) > N: 
            people[i][1] = 2*N - people[i][1] - yvel
        if (people[i][0] + xvel) < 0: 
            people[i][0] = abs(xvel) - people[i][0]
        if (people[i][1] + yvel) < 0: 
            people[i][1] = abs(yvel) - people[i][1]
        if [people[i][0] + xvel, people[i][1] + yvel] in people:
            print "Collision!"
            xvel = np.random.uniform(-velocity, velocity)
            yvel = np.random.uniform(-velocity, velocity)
        else:
            people[i][0] += xvel #If they aren't too close to the walls
            people[i][1] += yvel
    xvalues, yvalues, xcolor, ycolor = coordinates(people)
    for i in range(len(people)):
        if infected[i] == 1:
            xpos = xvalues[i]*np.ones(len(people)) #xpos has everyone's x position
            ypos = yvalues[i]*np.ones(len(people)) #ypos has everyone's y position
            dist = np.sqrt((xpos - xvalues)**2 + (ypos - yvalues)**2) #Calculate everyone's distance from infected person i
            infect = np.where(dist<2)[0] #Get indices where distance is too close
            for j in infect: #Infect them with 75% probability
                p = np.random.random()
                if (p < 0.75):
                    infected[j] = 1
    time.append(t+1)
    t += 1
    totalinf.append(sum(infected))
    x1 = ax1.scatter(xvalues, yvalues, c = 'green') #Plot everyone with green
    x2 = ax1.scatter(xcolor, ycolor, c = 'red', alpha = 0.7) #Overplot infected people with red so we end up with green = non-infected and red = infected
    plt.xlim(0, N)
    plt.ylim(0, N)
    plt.legend((x1, x2), ('Healthy', 'Infected'), scatterpoints = 1)


    
plt.show()
print t
print suminf

fig = plt.figure()
ax2 = fig.add_subplot(111)
ax2.scatter(xvalues, yvalues, c = 'black')
ax2.scatter(xcolor, ycolor, c = 'red') #I overplot the infected people with red
ax2.set_title("Position of people at final time step without buffer")
ax2.set_xlabel('X dimension')
ax2.set_ylabel('Y dimension')
plt.xlim(0, N)
plt.ylim(0, N)
plt.show()


#pdfinf is used to get the number of new infections at each timestep
pdfinf = []
for i in range(len(totalinf)-1):
    pdfinf.append(totalinf[i+1] - totalinf[i])

pdfinf.append(0)
pdfinf = np.array(pdfinf)
time = np.array(time)

#Plotting Total Number of People Infected vs Time
plt.plot([], [], linewidth = 0, label = "Total number of people = " + str(N))
plt.plot(time, totalinf, linewidth = 2, label = 'Number of People Infected', color = 'r')
plt.ylabel('Total Number of people infected')
plt.xlabel('Time steps')
plt.ylim(0, 100)
plt.title('Total Number of People Infected vs Time')
plt.legend(loc = 'lower right', prop={'size':10})
plt.show()

#Plotting the PDF of Infecting People
plt.bar(time, pdfinf, color = 'black')
plt.title("Number of New People Infected at Each Time Step")
plt.xlabel("Time Step")
plt.ylabel("New People Infected")
plt.show()
