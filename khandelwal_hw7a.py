import numpy as np
import time

#I run the program a bunch of times for different N to see how the answer changes with increasing N. I could have done with by defining an array of N's and looping over it but copying and pasting also works. 


#N = 1000
start = time.time()
counter = 0.0
N = 1000
for i in range(N):
    #We find N random cordinates with x and y between -1 and 1
    x = np.random.uniform(-1.0, 1.0)
    y = np.random.uniform(-1.0, 1.0)
    #Now we check to see if they lie within the circle by seeing whether their distance from the center is less than 1 (the radius of the circle)
    if np.sqrt((x**2) + (y**2)) <= 1.0:
        counter += 1.0

#Probability of finding a random coordinate inside the circle is p
p = counter/N
#Area of the circle = probability of random point inside circle * area of square
area = p*4.0
#Pi = Area of circle/radius**2 = Area/1**2
pi = area/1
end = time.time()
print "N = ", str(N), " pi = ", pi, "time = " + str(end - start)

#N = 10000
start = time.time()
counter = 0.0
N = 10000
for i in range(N):
    x = np.random.uniform(-1.0, 1.0)
    y = np.random.uniform(-1.0, 1.0)
    if np.sqrt((x**2) + (y**2)) <= 1.0:
        counter += 1.0

p = counter/N
area = p*4.0
pi = area/1
end = time.time()
print "N = ", str(N), " pi = ", pi, "time = " + str(end - start)


#N = 100000
start = time.time()
counter = 0.0
N = 100000
for i in range(N):
    x = np.random.uniform(-1.0, 1.0)
    y = np.random.uniform(-1.0, 1.0)
    if np.sqrt((x**2) + (y**2)) <= 1.0:
        counter += 1.0
p = counter/N
area = p*4.0
pi = area/1
end = time.time()
print "N = ", str(N), " pi = ", pi, "time = " + str(end - start)


#N = 1000000
start = time.time()
counter = 0.0
N = 1000000
for i in range(N):
    x = np.random.uniform(-1.0, 1.0)
    y = np.random.uniform(-1.0, 1.0)
    if np.sqrt((x**2) + (y**2)) <= 1.0:
        counter += 1.0
p = counter/N
area = p*4.0
pi = area/1
end = time.time()
print "N = ", str(N), " pi = ", pi, "time = " + str(end - start)


#N = 10000000, needed to get to 3.141
print "The next one  will take about 15 seconds to run but should get close to 3.141"
start = time.time()
counter = 0.0
N = 10000000
for i in range(N):
    x = np.random.uniform(-1.0, 1.0)
    y = np.random.uniform(-1.0, 1.0)
    if np.sqrt((x**2) + (y**2)) <= 1.0:
        counter += 1.0
p = counter/N
area = p*4.0
pi = area/1
end = time.time()
print "N = ", str(N), " pi = ", pi, "time = " + str(end - start)

print "We can see that the answer converges to the real value of pi with increasing N. We also see that the time increases linearly with N."

