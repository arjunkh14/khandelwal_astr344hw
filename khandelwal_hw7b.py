import numpy as np
import matplotlib.pyplot as plt
import time

t1 = time.time()
array1, xvalues, yvalues = [], [], []
sum = 0.0
N = 1000
first = 0.0
for i in range(1,100):
    xvalues.append(i)
    sum = 0.0
    for a in range(N):
        array1 = []
        for b in range(i):
            x = np.random.randint(1, 365)
            array1.append(x)
        if len(array1) != len(set(array1)):
                                 sum += 1.0
    p = sum/N
    yvalues.append(p)
    if p > 0.5:
        print i, p
        break

plt.plot(xvalues,yvalues)
plt.xlabel("Number of people")
plt.ylabel("Probability of any two having the same birthday")
plt.show()
