import numpy as np
import matplotlib.pyplot as plt

#Defining functions
def f(x):
    return np.sin(x)
def g(x):
    return (x**3)-x-2
def y(x):
    return -6 + x + (x**2)

#Defining max iterations and user toggle
maxit = 1000
maxit = input("What should be the max number of iterations? Default is 1000  ")

#Defining tolerance and user toggle
tol = 10**(-3)
tol = input("What should be tolerance? Default is 0.001  ")


#Defining a function which will find roots of a given function in a given range
def roots(func, a, b):
    if abs(func(a)) < tol:
        print "The root of %s in the range (%s, %s) is %s" % (func, xa, xb, a)
    if abs(func(b)) < tol:
        print "The root of %s in the range (%s, %s) is %s" % (func, xa, xb, b)
    else:
        while func(a)/func(b) > 0: #If the brackets have the same sign, we double their width
            a *= 0.5
            if abs(func(a)) < tol:
                print "The root of %s in the range (%s, %s) is %s" % (func, a, b, a)
                break
            b *= 2
            if abs(func(b)) < tol:
                print "The root of %s in the range (%s, %s) is %s" % (func, a, b, b)
                break
    if abs(func(a)) < tol:
        print "The root of %s in the range (%s, %s) is %s" % (func, a, b, a)
    if abs(func(b)) < tol:
        print "The root of %s in the range (%s, %s) is %s" % (func, a, b, b)
    xa = a
    xb = b
    c = (a + b)/2.0
    counter = 0
    mid = func(c)
    while abs(mid) > abs(tol):
        if func(a)/func(c) < 0:
            b = c
        if func(b)/func(c) < 0:
            a = c
        counter += 1
        c = (a + b)/2.0
        mid = func(c)
        if counter > maxit:
            print "Error: Your operation timed out. Please either incease iterations or tolerance"
            break
    if abs(mid) < abs(tol):
        print "The root of %s in the range (%s, %s) is %s" % (func, xa, xb, c)

roots(y, 0, 5)
roots(f, 2, 4)
roots(g, 1, 2)

#Getting ready to plot the functions
xrange = np.linspace(0, 5, 100)
xrangegx = np.linspace(1, 2, 100)
zeros = np.zeros(len(xrange))

fx, gx, yx = [], [], []
for i in xrange:
    fx.append(f(i))
    gx.append(g(i))
    yx.append(y(i))

plt.plot(xrange, fx, linewidth = 3, label = 'f(x) = Sin(x)')
plt.plot(xrange, yx, linewidth = 3, label = 'y(x) = -6 + x + x^2')
plt.plot(xrange, gx, linewidth = 3, label = 'g(x) = x^3 - x - 2')
plt.plot(xrange, zeros, linewidth = 3, label = "X Axis")
plt.legend(prop={'size':10})
plt.ylim([-2, 5])
plt.savefig("khandelwal_hw5.eps")



