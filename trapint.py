import numpy as np

def trapint(xi, fxi):
    l1 = len(xi)
    deltax = ((float(xi[l1 - 1]) - xi[0])/(l1-1))
    sum = 0
    for i in np.arange(0, l1-1):
        a = (deltax/2) * (fxi[i] + fxi[i+1])
        sum += a
    return sum
