import numpy as np
from scipy.optimize import curve_fit
from scipy import integrate 
import matplotlib.pyplot as plt

def gaussian(x, A, mu, sigma):
    return A * np.exp(-(x - mu)**2/(2.*sigma**2))

def d_gauss(x, a1, mu1, sigma1, a2, mu2, sigma2):        
    return (a1 * np.exp(-(x - mu1)**2/(2.*sigma1**2))) + (a2 * np.exp(-(x - mu2)**2/(2.*sigma2**2)))

va, ta = np.loadtxt('hw10a.dat', usecols = (0, 1), unpack = True)

plt.plot(va, ta, 'o')
plt.show()



print "Doing Part 1 - Single Gaussian"

p0 = [0.002, 0., 1.]
params1, pcov1 = curve_fit(gaussian, va, ta, p0 = p0)
print "parameters(A, mu, sigma) = ", str(params1)
print "covariance matrix = ", str(pcov1)

p11, p12, p13 = params1[0], params1[1], params1[2]

vnew1 = np.linspace(np.min(va), np.max(va), 1000)
tfit1 = gaussian(vnew1, p11, p12, p13)

print "FWHM = ", str(2.355*p13)
ya, yaerr = integrate.quad(gaussian, np.min(va), np.max(va), args = (p11, p12, p13))
print "Area under fitted curve = ", str(ya)

plt.plot(va, ta, 'o', vnew1, tfit1)
plt.xlabel('Velocity (km/s)')
plt.ylabel('Temperature')
plt.show()


print "Doing part 2 - Double Gaussian"

print "Trying a single gaussian"

vb, tb = np.loadtxt('hw10b.dat', usecols = (0, 1), unpack = True)

p0 = [0.002, 0., 1.]
params2, pcov2 = curve_fit(gaussian, vb, tb, p0 = p0)

print "parameters(A, mu, sigma) = ", str(params2)
print "covariance matrix = ", str(pcov2)

p21, p22, p23 = params2[0], params2[1], params2[2]

vnew2 = np.linspace(np.min(vb), np.max(vb), 1000)
tfit2 = gaussian(vnew2, p21, p22, p23)

print "FWHM = ", str(2.355*p23)
ya, yaerr = integrate.quad(gaussian, np.min(vb), np.max(vb), args = (p21, p22, p23))
print "Area under fitted curve = ", str(ya)

plt.plot(vb, tb, 'o', vnew2, tfit2)
plt.xlabel('Velocity (km/s)')
plt.ylabel('Temperature')
plt.show()



print "Trying a double gaussian"

p0 = [0.002, 0., 1., 0.002, 0., 1.]
params3, pcov3 = curve_fit(d_gauss, vb, tb, p0 = p0)

print "parameters(a1, mu1, sigma1, a2, mu2, sigma2) = ", str(params3)
print "covariance matrix = ", str(pcov3)

p31, p32, p33, p34, p35, p36 = params3[0], params3[1], params3[2], params3[3], params3[4], params3[5]

vnew3 = np.linspace(np.min(vb), np.max(vb), 1000)
tfit3 = d_gauss(vnew2, p31, p32, p33, p34, p35, p36)

print "FWHM_1 = ", str(2.355*p33), "FWHM_2 = ", str(2.355*p36)
ya, yaerr = integrate.quad(d_gauss, np.min(vb), np.max(vb), args = (p31, p32, p33, p34, p35, p36))
print "Area under fitted curve = ", str(ya)

plt.plot(vb, tb, 'o', vnew3, tfit3)
plt.xlabel('Velocity (km/s)')
plt.ylabel('Temperature')
plt.show()







"""
def func(x, a, b, c):
    return a * np.exp(-b*x) + c

x = np.linspace(0, 4, 50)
y = func(x, 2.5, 1.3, 0.5)
yn = y + 0.2 * np.random.normal(size = len(x))

popt, pcov = curve_fit(func, x, yn)

print popt, pcov

p1, p2, p3 = popt[0], popt[1], popt[2]
resid = yn - func(x, p1, p2, p3)
fres = sum(resid**2)
print fres

yfit = func(x, p1, p2, p3)

plt.plot(x, yn,'o', x, yfit)
#plt.plot(x, yfit, color = 'red')
plt.show()
"""
